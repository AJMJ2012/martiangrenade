using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Microsoft.Xna.Framework;
using System;

namespace MartianGrenade {
	public class MartianGrenade : Mod {
		public MartianGrenade() {
			Properties = new ModProperties {
				Autoload = true,
				AutoloadGores = true,
				AutoloadSounds = true
			};
		}
	}

	public class MartianGrenadeItem : ModItem {
		public override void SetStaticDefaults() {
			DisplayName.SetDefault("Electrosphere Grenade");
		}
		public override void SetDefaults() {
			item.autoReuse = false;
			item.consumable = true;
			item.damage = 40;
			item.height = 20;
			item.knockBack = 2f;
			item.maxStack = 999;
			item.noMelee = true;
			item.noUseGraphic = true;
			item.rare = 8;
			item.shoot = mod.ProjectileType("MartianGrenadeProjectile");
			item.shootSpeed = 5.5f;
			item.thrown = true;
			item.useAnimation = 44;
			item.UseSound = SoundID.Item1;
			item.useStyle = 5;
			item.useTime = 44;
			item.value = Item.sellPrice(0, 0, 2, 75);
			item.width = 20;
		}
		public override void AddRecipes() {
			ModRecipe recipe = new ModRecipe(mod);
			recipe.AddIngredient(ItemID.Grenade, 1);
			recipe.AddIngredient(ItemID.Nanites, 1);
			recipe.AddIngredient(ItemID.MartianConduitPlating, 1);
			recipe.AddTile(TileID.Anvils);
			recipe.SetResult(this, 1);
			recipe.AddRecipe();
		}
	}

	public class MartianGrenadeProjectile : ModProjectile {
		public override void SetStaticDefaults() {
			DisplayName.SetDefault("Electrosphere Grenade");
		}
		public override void SetDefaults() {
			for (int j = 0; j < 2; j++) { projectile.ai[j] = 0f; }
			projectile.width = 20;
			projectile.height = 20;
			projectile.friendly = true;
			projectile.penetrate = 1;
			projectile.thrown = true;
			projectile.timeLeft = 180;
			projectile.aiStyle = 14;
		}

		public override void AI() {
			int index2 = Dust.NewDust(projectile.position, projectile.width, projectile.height, 135, 0.0f, 0.0f, 100, new Color(), 1f);
			Main.dust[index2].noGravity = false;
		}

		public override void Kill(int timeLeft) {
			Main.PlaySound(2, (int)projectile.position.X, (int)projectile.position.Y, 94);
			for (int index1 = 0; index1 < 20; ++index1) {
				int index2 = Dust.NewDust(projectile.position, projectile.width, projectile.height, 226, 0.0f, 0.0f, 100, new Color(), 0.5f);
				Main.dust[index2].velocity *= 2.0f;
				Main.dust[index2].velocity.Y--;
				Main.dust[index2].position = Vector2.Lerp(Main.dust[index2].position, projectile.position, 0.5f);
			}
			for (int index1 = 0; index1 < 10; ++index1) {
				int index2 = Dust.NewDust(projectile.position, projectile.width, projectile.height, 135, 0.0f, 0.0f, 100, new Color(), 2.1f);
				Main.dust[index2].velocity *= 3f;
				Main.dust[index2].noGravity = true;
			}
			if (Main.myPlayer == projectile.owner) {
				Rectangle rectangle = new Rectangle((int)projectile.Center.X - 40, (int)projectile.Center.Y - 40, 80, 80);
				for (int index = 0; index < 1000; ++index) {
					if (index != projectile.whoAmI && Main.projectile[index].active && (Main.projectile[index].owner == projectile.owner && Main.projectile[index].type == 443) && Main.projectile[index].getRect().Intersects(rectangle)) {
						Main.projectile[index].ai[1] = 1f;
						Main.projectile[index].velocity = (projectile.Center - Main.projectile[index].Center) / 5f;
						Main.projectile[index].netUpdate = true;
					}
				}
				Projectile.NewProjectile(projectile.Center.X, projectile.Center.Y, 0.0f, 0.0f, 443, projectile.damage, 0.0f, projectile.owner, 0.0f, 0.0f);
			}
		}
	}

	public class MartianGrenadeDrop : GlobalNPC {
		public override void NPCLoot(NPC npc) {
			switch (npc.type) {
				case NPCID.MartianOfficer:
				case NPCID.MartianEngineer:
					if (Main.rand.Next(8) == 0) {
						Item.NewItem((int) npc.position.X, (int) npc.position.Y, npc.width, npc.height, mod.ItemType("MartianGrenade"), Main.rand.Next(8, 21), false, 0, false, false);
					}
					break;
			}
		}
	}
}